terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "4.21.0"
        }
        azure = {
            source = "hashicorp/azurerm"
            version = "3.12.0"
        }
        google = {
            source = "hashicorp/google"
            version = "4.27.0"
        }
        kubernetes = {
            source = "hashicorp/kubernetes"
            version = "2.12.0"
        }
        linode = {
            source = "linode/linode"
            version = "1.28.0"
        }
        digitalocean = {
            source = "digitalocean/digitalocean"
            version = "2.21.0"
        }
        oci = {
            source = "oracle/oci"
            version = "4.82.0"
        }
        pnap = {
            source = "phoenixnap/pnap"
            version = "0.14.0"
        }
        alicloud = {
            source = "aliyun/alicloud"
            version = "1.173.0"
        }
        intersight = {
            source = "CiscoDevNet/intersight"
            version = "1.0.28"
        }
        proxmox = {
          source  = "telmate/proxmox"
          version = ">= 2.9.11"
        }
        vsphere = {
          source  = "hashicorp/vsphere"
          version = ">= 2.2.0"
        }
    }
}
