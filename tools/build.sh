#!/bin/bash

set -x
set -e

# what we are building (eg "runners")
thing=$1
# optional single runner to build (eg "napalm-runner")
runner=$2
VERSION=$UPSTREAM_VERSION

docker=docker
if which podman &>/dev/null; then
  docker=podman
fi

case $thing in
	runners)
		VERSION=$(./tools/get-version.sh)
		projects=${runner:-"$(ls -d *-runner)"}
		registry=no
	;;
	drp)
		projects=dr-provision
		registry=yes
		src="https://s3-us-west-2.amazonaws.com/rebar-catalog/drp/$VERSION.amd64.linux.zip"
		url=$(echo $src | sed 's/+/%2B/g')
		curl -ksfL $url -o dr-provision/drp.tar.gz
		tar xzf dr-provision/drp.tar.gz --strip-components=3 -C ./dr-provision bin/linux/amd64/drpcli bin/linux/amd64/dr-provision bin/linux/amd64/dr-waltool
	;;
	drpcli)
		projects=drpcli
		registry=yes
		src="https://s3-us-west-2.amazonaws.com/rebar-catalog/drpcli/$VERSION/amd64/linux/drpcli"
		url=$(echo $src | sed 's/+/%2B/g')
		curl -ksfL $url -o drpcli/drpcli
		chmod +x drpcli/drpcli
	;;
	rackngo)
		if [[ "$CI_COMMIT_REF_NAME" != "main" ]]; then
			VERSION=$(head -1 ./rackngo/Dockerfile | cut -f2 -d:)_dev
		else
			VERSION=v$(head -1 ./rackngo/Dockerfile | cut -f2 -d:)
		fi
		registry=yes
		projects=rackngo
	;;
	*)
		echo No matching argument
		echo exit 1
	;;
esac

sys=$(uname -s)
case $sys in
  Linux)  sha="sha256sum" ;;
  Darwin) sha="shasum -a 256" ;;
  *)      echo "Unsupported build platform '$sys'"; exit 1 ;;
esac

dip() (
   cd "$project" && $docker "$@"
)

mkdir -p containers

for project in $projects; do
   if ! [[ -f $project/Dockerfile ]]; then
     echo "No Dockerfile for $project, skipping"
     continue
  fi
	echo "Building $project"
	ver=$(echo $VERSION | sed 's/+g/_/g')
  dip build --pull -t $project:$ver .
  [[ $CI_REGISTRY ]] || continue
	if [[ "$registry" == "no" ]]; then
		dip tag $project:$ver $CI_REGISTRY/rackn/containers/$project:$ver
		dip save $project:$ver -o ../containers/${project}_$ver.tar
		(cd containers && gzip ${project}_$ver.tar &&	$sha ${project}_$ver.tar.gz >> containers_$ver.txt)
	fi
  if [[ "$CI_COMMIT_BRANCH" == "main" && "$registry" == "yes" ]]; then
	  dip tag $project:$ver $CI_REGISTRY/rackn/containers/$project:$ver
		dip push $CI_REGISTRY/rackn/containers/$project:$ver
		if [[ $ver =~ "_" ]]; then
			dip tag $project:$ver $CI_REGISTRY/rackn/containers/$project:tip
			dip push $CI_REGISTRY/rackn/containers/$project:tip
		fi
		if [[ $ver =~ ^v[0-9]+.[0-9]+ ]]; then
		  dip tag $project:$ver $CI_REGISTRY/rackn/containers/$project:stable
			dip push $CI_REGISTRY/rackn/containers/$project:stable
		fi
	fi
done

re='^v[0-9]+(\.[0-9]+)*$';
if [[ $CI_COMMIT_BRANCH == "main" || $CI_COMMIT_TAG =~ $re ]]; then
	aws s3 cp containers/ s3://get.rebar.digital/containers/ --recursive --acl public-read
fi

if [ -n "$err" ]; then
	exit 66
fi
