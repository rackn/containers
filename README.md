# RackN Containers

This project creates and publishes containers for use with Digital Rebar.

## Available containers

### Development containers

registry.gitlab.com/rackn/containers/rackn-base

### DRP containers

registry.gitlab.com/rackn/containers/drpcli
registry.gitlab.com/rackn/containers/dr-provision

### Runner containers

registry.gitlab.com/rackn/containers/drpcli-runner
registry.gitlab.com/rackn/containers/ansible-runner
registry.gitlab.com/rackn/containers/terraform-runner
registry.gitlab.com/rackn/containers/govc-runner
registry.gitlab.com/rackn/containers/vmware-tools-runner
registry.gitlab.com/rackn/containers/guacd-runner

## Build/Test Process

    NAME="mycli"
    IMAGE="$NAME_v0.0.1"
    docker build . -t $NAME
    docker save $NAME:latest -o $IMAGE.tar.gz
    drpcli files upload ${IMAGE}.tar.gz as contexts/docker-context/$IMAGE
    drpcli plugins runaction docker-context imageUpload context/image-name $IMAGE context/image-path files/contexts/docker-context/$IMAGE > /dev/null
