# awscli container

This container provides the AWS CLI and the minimal requirements for
running DRPCLI (but not DRPCLI)

We've modified this compared to their Github source it includes the download curl and also includes `which` in the install (needed by DRP workflows)

AWS Sources:

- https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-docker.html
- https://github.com/aws/aws-cli/blob/v2/docker/Dockerfile
