# drpcli container

This container provides just the drpcli binary.  You can pass environment variables.

example:
```bash
$ docker run --rm --network=host -e RS_ENDPOINT=https://localhost:8092 -it registry.gitlab.com/rackn/containers/drpcli:latest machines list
```
