# napalm-runner container

This container provides a napalm-runner container for network device automation.

example:
```bash
$ docker run --rm --network=host -e RS_ENDPOINT=https://localhost:8092 -it registry.gitlab.com/rackn/containers/napalm:latest machines list
```
