# drpcli-runner container

This container provides a drpcli-runner container.

example:
```bash
$ docker run --rm --network=host -e RS_ENDPOINT=https://localhost:8092 -it registry.gitlab.com/rackn/containers/drpcli:latest machines list
```
